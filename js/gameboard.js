import { Walls, OppositeWalls } from "./directions.js"
import Mouse from "./mouse.js"
import Point from "./point.js"

/**
 * This class represents the GameBoard. Provides methods for initializing, manipulating and rendering the current state of the board.
 */
class GameBoard {
    canvas
    ctx
    grid
    tileSize

    // maze variables
    mazeWidth
    mazeHeight
    totalTiles
    exitCell
    longestDistance
    pathDistance
    mazeCreated
    mice = []
    depths = {}
    score
    isOver

    showMaze = true

    checkHighScore
    updateScore

    constructor(canvas, checkHighScore, updateScore) {
        this.canvas = canvas
        this.ctx = canvas.getContext('2d');
        this.checkHighScore = checkHighScore
        this.updateScore = updateScore
    }

    /**
     * Initializes the board. Can be use to reset the board as well.
     * @param {number} width width of the game board
     * @param {number} height height of the game board
     * @param {number} size size of single tile
     */
    init(width, height, size) {
        this.score = 0;
        this.isOver = false;
        this.tileSize = size;
        this.mazeWidth = width;
        this.mazeHeight = height;
        this.totalTiles = width * height;

        this.canvas.width = width * size;
        this.canvas.height = height * size;

        this.mice = [];
        this.depths = {};

        this.mice.push(new Mouse(this));

        this.showMaze = true;
        this.mazeCreated = false;
        this.longestDistance = 0;
        this.pathDistance = 0;
        this.exitCell = null;
        this.createGrid();
    }

    /**
     * Moves player by one tile in given direction.
     * @param {string} direction
     */
    move(direction) {
        this.mice.forEach(mouse => {
            mouse.goTo(direction, this.exitCell);
        });
    }

    /**
     * Build the maze. The board has to be already initialized.
     */
    initBuildMaze() {
        this.buildMaze(new Point(0,0));
    }

    incrementScore() {
        this.score++;
        this.updateScore(this.score)
    }

    buildMaze(point) {
        //generate the longest possible path and put exit there
        let nextPoint;
        this.pathDistance++;
        point.directions.forEach(direction => {
            nextPoint = new Point(point.x + direction.x, point.y + direction.y);
            if (this.inGridRange(nextPoint.x, nextPoint.y) && this.gridAt(nextPoint.x, nextPoint.y) === 0) {
                this.grid[point.y][point.x] |= Walls[direction.key];
                this.grid[nextPoint.y][nextPoint.x] |= OppositeWalls[direction.key];
                this.buildMaze(nextPoint);
            }

        });
        if (this.pathDistance > this.longestDistance) {
            // this path is longer that the currently known longest path. let's move the exit
            this.longestDistance = this.pathDistance;
            if (this.exitCell) {
                this.grid[this.exitCell.y][this.exitCell.x] ^= Walls.EXIT;
            }
            this.exitCell = point;
            this.grid[this.exitCell.y][this.exitCell.x] |= Walls.EXIT;
        }
        if (!this.depths.hasOwnProperty(this.pathDistance)) {
            this.depths[this.pathDistance] = [];
        }
        this.depths[this.pathDistance].push(point);
        this.pathDistance--;
        if (this.pathDistance === 0) {
            this.mazeCompleted()
        }
    }

    gridAt(x, y, value) {
        if (value) {
            this.grid[y][x] = value;
        }
        return this.grid[y][x];
    }

    inGridRange(x, y) {
        return x >= 0 && x < this.mazeWidth && y >= 0 && y < this.mazeHeight;
    }

    createGrid() {
        this.grid = [];
        for (let y = 0; y < this.mazeHeight; y++) {
            this.grid[y] = [];
            for (let x = 0; x < this.mazeWidth; x++) {
                this.grid[y].push(Walls.I);
            }
        }
    }

    /**
     * Draws the current state of the board to the canvas.
     */
    draw() {
        this.ctx.fillStyle = '#e5e5e5'
        this.ctx.fillRect(0, 0, 1000, 1000);
        if (this.mazeCreated) {
            this.drawMouse(this.ctx);
        }

        for (let y = 0; y < this.mazeHeight; y++) {
            for (let x = 0; x < this.mazeWidth; x++) {
                let cell = this.gridAt(x, y);
                this.ctx.save();

                this.ctx.translate(x * this.tileSize, y * this.tileSize);
                this.ctx.beginPath();
                if (this.showMaze) {
                    this.ctx.strokeStyle = '#333';
                    if (cell === 0) {
                        this.drawEastWall();
                        this.drawSouthWall();
                        this.drawTile();
                    } else {
                        if (!(cell & Walls.S)) {
                            this.drawSouthWall();
                        }
                        if (!(cell & Walls.E)) {
                            this.drawEastWall();
                        }
                        if (cell & Walls.EXIT) {
                            this.drawFinish();
                        }
                    }
                }

                this.ctx.stroke();
                this.ctx.restore();
            }
        }

    }

    drawEastWall() {
        this.ctx.moveTo(this.tileSize, 0);
        this.ctx.lineTo(this.tileSize, this.tileSize);
    }

    drawSouthWall() {
        this.ctx.moveTo(0, this.tileSize);
        this.ctx.lineTo(this.tileSize, this.tileSize);
    }

    drawFinish() {
        this.ctx.fillStyle = '#000000';
        this.ctx.fillRect(this.tileSize / 3, this.tileSize / 3, this.tileSize / 4, this.tileSize / 4);
    }

    drawTile() {
        this.ctx.save();
        this.ctx.globalAlpha = 0.5;
        this.ctx.fillStyle = '#999';
        this.ctx.fillRect(0, 0, this.tileSize, this.tileSize);
        this.ctx.restore();
    }

    drawMouse() {
        this.mice.forEach(mouse => {

            this.ctx.save();

            this.ctx.fillStyle = mouse.color;
            this.ctx.strokeStyle = '#fff';
            this.ctx.lineWidth = 2;

            this.ctx.beginPath();
            this.ctx.translate(mouse.pos.x * this.tileSize, mouse.pos.y * this.tileSize);

            this.ctx.rect(0, 0, this.tileSize, this.tileSize);
            this.ctx.fill();
            this.ctx.stroke();
            this.ctx.restore();
            if (mouse.solved && !this.isOver) {
                // we want to save the highscore after the draw method, hence we use timeout, to do it asynchronously, after the drawing is done
                window.setTimeout(() => this.checkHighScore(this.score), 0);
                this.isOver = true
            }

        });
    }

    mazeCompleted() {
        this.mazeCreated = true;
        this.grid[0][0] |= Walls.ENTRY;
    }
}

export default GameBoard