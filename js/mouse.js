import Point from "./point.js";
import { SeekLookup, Walls } from "./directions.js"

let moveAudio = new Audio("audio/mixkit-quick-jump-arcade-game-239.wav"),
    winAudio = new Audio("audio/mixkit-animated-small-group-applause-523.wav");

/**
 * This class represents the state of one mouse.
 */
class Mouse {

    constructor(gameBoard) {
        this.gameBoard = gameBoard
        this.pos = new Point(0, 0);
        this.history = [];
        this.solved = false;
        this.color = 'black';
    }

    goTo(dir, exitCell) {
        //moves mouse to the next position
        if (this.solved) {
            // if we are already at the end, ignore all move commands
            return;
        }

        let nextPos;

        //assign position in grid
        let currentCell = this.gameBoard.gridAt(this.pos.x, this.pos.y);

        let isPossibleToGoThere = this.getPossible(currentCell).some(v => v.key === dir);
        if (isPossibleToGoThere) {
            // it is possible to execute move in the desired direction
            moveAudio.play();
            let nextDirection = SeekLookup[dir];
            nextPos = new Point(this.pos.x + nextDirection.x, this.pos.y + nextDirection.y);
            // successfuly executed move should increment the score
            this.gameBoard.incrementScore();

            let historyObject = {
                pos: this.pos,
            };
            this.history.push(historyObject);
            this.pos = nextPos;

            //check if next position is the exit
            if (nextPos.x === exitCell.x && nextPos.y === exitCell.y) {
                winAudio.play();
                this.solved = true;
            }
        }
    }

    getPossible(cell) {
        return [{
            key: 'N',
            value: !!(cell & Walls.N)
        }, {
            key: 'S',
            value: !!(cell & Walls.S)
        }, {
            key: 'E',
            value: !!(cell & Walls.E)
        }, {
            key: 'W',
            value: !!(cell & Walls.W)
        },].filter(function (d) {
            return d.value;
        });
    }

}


export default Mouse