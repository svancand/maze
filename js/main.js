import GameBoard from "./gameboard.js";
'use strict';

const NO_OF_HIGH_SCORES = 10;
let rowsInput,
    colsInput,
    highScoreList,
    startBtn,
    scoreTxt,
    span,
    modal,
    modalBtn,
    validationTxt,
    tapeTxt;

const gameboard = new GameBoard(document.getElementById("maze"), checkHighScore, (score) => scoreTxt.innerHTML = "Your score: " + score)

bindView();
showHighScores()

modalBtn.onclick = function() {
    modal.style.display = "block";
}
span.onclick = function() {
    modal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
}

if (navigator.onLine) {
    tapeTxt.innerHTML = "You are online!";
} else {
    tapeTxt.innerHTML = "You are offline!";
}

window.addEventListener('offline', function(e) { tapeTxt.innerHTML = "You are offline!"; });

window.addEventListener('online', function(e) { tapeTxt.innerHTML = "You are online!"; });

//initialise maze after clicking on Start button
startBtn.addEventListener('click', function () {
    scoreTxt.innerHTML = "Your score: 0";
    validationTxt.innerHTML = "";

    //validating input
    if (isNaN(rowsInput.value) || isNaN(colsInput.value) || rowsInput.value < 2 || rowsInput.value > 20 || colsInput.value < 2 || colsInput.value > 20) {
        validationTxt.innerHTML = "This is not valid!";
    } else {
        gameboard.init(rowsInput.value, colsInput.value, 40);
        gameboard.initBuildMaze();
    }
});

//moving through maze using arrow keys
function checkKey(e) {
    e = e || window.event;
    let dir
    if (e.keyCode == '38') {
        // up arrow
        dir = 'N'
    } else if (e.keyCode == '40') {
        // down arrow
        dir = 'S'
    } else if (e.keyCode == '37') {
        // left arrow
        dir = 'W'
    } else if (e.keyCode == '39') {
        // right arrow
        dir = 'E'
    }
    gameboard.move(dir)
}

document.addEventListener("keydown", checkKey);

function update() {
    gameboard.draw();
    // schedule the function to be called before the next repaint, we strive to get those sweet 60 fps
    window.requestAnimationFrame(() => update());
}

gameboard.init(40, 40, 10);
update();


function checkHighScore(score) {
    const highScores = JSON.parse(localStorage.getItem('highScores')) ?? [];

    // pick the lowest score or zero if there is none
    const lowestScore = highScores[NO_OF_HIGH_SCORES - 1]?.score ?? 0;

    if (score > lowestScore) {
        saveHighScore(score, highScores);

    } else {
        alert("Wow, that was fast! Congratz for winning! I won't be adding you to the sloth table, don't worry.");
    }
    showHighScores();

}

function saveHighScore(score, highScores) {
    let name = prompt('Wow, you are slow! What\'s your name?');
    if(name == null){
        return;
    }
    while(name === ""){
        name = prompt('No, seriously, tell me your name!');
    }
    const newScore = {score, name};

    // 1. Add to list
    highScores.push(newScore);

    // 2. Sort the list
    highScores.sort((a, b) => b.score - a.score);

    // 3. Select new list
    highScores.splice(NO_OF_HIGH_SCORES);

    // 4. Save to local storage
    localStorage.setItem('highScores', JSON.stringify(highScores));
}

function showHighScores() {
    const highScores = JSON.parse(localStorage.getItem('highScores')) ?? [];

    // generate ordered list with highscores
    highScoreList.innerHTML = highScores
        .map((score) => `<li><b> ${score.name}</b> (${score.score}  steps)`)
        .join('');
}


function bindView() {
    // assing all dom variables
    rowsInput = document.getElementById('rows');
    colsInput = document.getElementById('cols');
    highScoreList = document.getElementById('highScores');
    startBtn = document.getElementById('startBtn');
    scoreTxt = document.getElementById('score');
    validationTxt = document.getElementById('inputValidation');
    modalBtn = document.getElementById('modalBtn');
    modal = document.getElementById('myModal');
    span = document.getElementsByClassName('close')[0];
    tapeTxt = document.getElementById('tapeTxt');
}
