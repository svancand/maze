// bit masks
let Walls = {
    I: 0,
    N: 1,
    S: 2,
    E: 4,
    W: 8,
    ENTRY: 16,
    EXIT: 32,
    VISITED: 64
};

let SeekDirections = [{
    key: 'N',
    x: 0,
    y: -1
}, {
    key: 'S',
    x: 0,
    y: 1
}, {
    key: 'E',
    x: 1,
    y: 0
}, {
    key: 'W',
    x: -1,
    y: 0
}];

let OppositeWalls = {
    N: Walls.S,
    S: Walls.N,
    E: Walls.W,
    W: Walls.E
};


let SeekLookup = {
    'N': SeekDirections[0],
    'S': SeekDirections[1],
    'E': SeekDirections[2],
    'W': SeekDirections[3],
};

export { Walls, SeekDirections, OppositeWalls, SeekLookup }