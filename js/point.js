import { SeekDirections } from "./directions.js";

/**
 * This class represents a 2d point that has a list of directions with 'random' priority for building purposes.
 */
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.directions = this.shuffle(SeekDirections.slice(0));
    }

    shuffle(array) {
        let counter = array.length,
            temp, index;

        // While there are elements in the array, we haven't processed yet
        while (counter > 0) {
            // Pick a random index
            index = Math.floor(Math.random() * counter);

            // Decrease counter by 1
            counter--;

            // And swap the last element with it
            temp = array[counter];
            array[counter] = array[index];
            array[index] = temp;
        }

        return array;
    }
}

export default Point